# Chat-Application

A client - server based chat application like whatsapp built on java.

## Description
This application is built with the help of **TCP/IP Proctocol**.<br>These protocols are used with the help of low-level-networking in java i.e with the help of **Sockets**.

## How it works
If client A wants to send message to client B, since client A doesn't know the address of client B so it cannot send the message directly.<br>
Hence client A sends the message to the server who finds client B's IP address and forwards the message to client B.<br>
So, the server maintains the list of addresses of all the clients that are connected to system.<br>
If the client is not connected the server itself maintains the pending messages of client as well as status of that messages.

## Functionalities

There are different set of functionalities provided by this application :-

##### 1) AUTHENTICATION - 
A **login** is provided for the maintaing the security of the application.<br>
The same username can't login again if it is already login not logout till now.

##### 2) REGISTER - 
A new user can **register** himself in this application.

##### 3) USER STATUS - 
It also shows the users that are currently active (i.e **online**) and currently not active (i.e **offline**) at that moment.

##### 4) MESSAGE STATUS - 
It also shows the status of the message i.e at that point the message has been **sent** or **delivered** or **read** by the user.

##### 5) CHAT BACKUP - 
It stores all the messages of each and every client with the help of **XML tags** at the client side so that whenever the user closes the application the messages don't get deleted!!!.
And messages store at server side in the absence of client (i.e when the client is offline) and delivered to client when it comes online.

##### 6) ENCRYPTION AND DECRYPTION -
The encryption and descryption is done with the help of **DES(Data Encryption Standard)!!!**<br>
The messages are encrypted and then send to server and when the fowards it another client that client decryptes the message to get the original one.<br> 
The messages saved in the backup are too encrypted!!!

## Some Screenshots of ChatSystem :-

**1. Login Page**

![](screenshots/LoginPage.png)

**2. Dashboard**

![](screenshots/Dashboard.png)

**3. ChatWindow**

![](screenshots/ChatWindowTalk.png)

**4. Messages in the ChatWindow**

![](screenshots/ChatWindowTalkRead.png)