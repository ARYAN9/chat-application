/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.ui;

import chatsystem.backend.client.clientconstants.ClientConstants;
import chatsystem.backend.client.cryptography.desoperations.DESWorking;
import chatsystem.backend.client.operations.ClientCode;
import chatsystem.backend.client.operations.ClientSend;
import chatsystem.backend.helper.filehandling.FileOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Aryan Dhami
 */
public class Dashboard extends javax.swing.JFrame implements ClientConstants{

    private final String username;
    private ClientCode clientCode;
    private HashMap<String,ContactComponent> allContacts;
    private final String clientPath;
    
    /**
     * Creates new form Dashboard
     */
    
    /**
     * This constructor just initializes all the members.
     * @param username : The username who is the owner of the dashboard (i.e name of client).
     */
    public Dashboard(String username) {
        this.username = username;
        clientPath = "C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+username+".xml";
        this.setTitle(username+"'s"+" Chat Box");//Setting the Frame title!!!
        allContacts = new HashMap<>();
        initComponents();
        chat_inbox_header.setText(username+"'s Chat Inbox");
        all_contact_panel.setLayout(new BoxLayout(all_contact_panel, BoxLayout.Y_AXIS));
        clientCode = new ClientCode(username, this);
    }
    
    /**
     * This method separates the online and offline users from allUsers send by server and accordingly shows them in UI!!!
     * This method then understands what type of pending message is given by server when the client was offline 
     * and then accordingly performs some File Handling if needed for backup 
     * and passes it to the parseXML() method for decoding it and showing it on UI!!!
     * @param allUsers : This is the String of all the online users which is send by the server and this String has special format:- allOnlineUsers::allOfflineUsers
     * @param pendingMessagesFileData : This is the String which contains the XML tags and some status from server which was received to user when the client was offline.
     */
    public void customInit(String allUsers,String pendingMessagesFileData){
        int midIndex = allUsers.indexOf("::");//::Aryan:Krishna:mihir
        String onlineUsers = allUsers.substring(0,midIndex);
        String offlineUsers = allUsers.substring(midIndex+2);
        
        String[] allOnlineUsersArray = onlineUsers.split(":");
        String[] allOfflineUsersArray = offlineUsers.split(":");

        if(!(allOnlineUsersArray.length == 1 && allOnlineUsersArray[0].equals(""))){//In case there is no online i.e first user!!!
            for(String user : allOnlineUsersArray){
                if(!username.equals(user)){
                    ContactComponent panel = new ContactComponent(user,username,clientCode);
                    allContacts.put(user,panel);
                    panel.setContact_latest_msg("online");
                    all_contact_panel.add(panel);
                }
            }    
        }
        for(String user : allOfflineUsersArray){//offline user array will never remain "" i.e. empty it will always have the Username's name!!!
            if(!username.equals(user)){
                ContactComponent panel = new ContactComponent(user,username,clientCode);
                allContacts.put(user,panel);
                panel.setContact_latest_msg("offline");
                all_contact_panel.add(panel);
            }
        }
        
        //When the server will not have any pending messages of the user then the server will send the blank string!!!

        String clientInfo = FileOperations.readFile(clientPath);
        if(pendingMessagesFileData.indexOf("<chat-conv>") == -1 && !"".equals(pendingMessagesFileData)){
            clientInfo = clientInfo + pendingMessagesFileData;
            parseXML(clientInfo);
        }else if(!"".equals(clientInfo) && pendingMessagesFileData.equals("")){
            parseXML(clientInfo);
        }else if(pendingMessagesFileData.indexOf("<chat-conv>") != -1 && clientInfo==""){
            clientCode.getFileOperationsObj().writeFile(clientPath, pendingMessagesFileData);
            parseXML(pendingMessagesFileData);
        }else if(clientInfo != "" && pendingMessagesFileData.indexOf("<chat-conv>") != -1){
            //Dono mai chat-conv
            int index = pendingMessagesFileData.indexOf("R:");
            String readString = "";
            while(index != -1){
                readString = readString + pendingMessagesFileData.substring(index,pendingMessagesFileData.indexOf(",",index)+1);
                index = pendingMessagesFileData.indexOf("R:",index+1);
            }
            String deliveredString = "";
            index = pendingMessagesFileData.indexOf("D:");
            while(index != -1){
                deliveredString = deliveredString + pendingMessagesFileData.substring(index,pendingMessagesFileData.indexOf(",",index)+1);
                index = pendingMessagesFileData.indexOf("D:",index+1);
            }
            parseXML(readString + deliveredString + clientCode.getFileOperationsObj().putPendingMessagesIntoClientData(pendingMessagesFileData,clientInfo,clientPath));
        }
    }
    
    /**
     * This method decodes the data passed to it and perform appropriate File Operations and accordingly shows on UI!!! 
     * @param pendingMessagesFileData : This is the String that contains the data that has to be parsed and shown on the UI of Dashboard!!!
     */
    public void parseXML(String pendingMessagesFileData){
        String beforePart = pendingMessagesFileData.substring(0,pendingMessagesFileData.indexOf("<chat-conv>"));
        String arrayOfUsers[] = beforePart.split(",");
        
        ArrayList<String> deliveredUsers = new ArrayList<>();
        ArrayList<String> readUsers = new ArrayList<>();
        
        int index = pendingMessagesFileData.indexOf("D:");
        while(index != -1){
            deliveredUsers.add(pendingMessagesFileData.substring(index+2,pendingMessagesFileData.indexOf(",",index+2)));
            index = pendingMessagesFileData.indexOf("D:",index+1);
        }
        
        index = pendingMessagesFileData.indexOf("R:");
        while(index != -1){
            readUsers.add(pendingMessagesFileData.substring(index+2,pendingMessagesFileData.indexOf(",",index+2)));
            index = pendingMessagesFileData.indexOf("R:",index+1);
        }
        
        for(int i=0;i<arrayOfUsers.length;i++){
            String user = arrayOfUsers[i];
            String pattern = "<"+user+">(.*?)</"+user+">";
            if(user.contains("R:") || user.contains("D:")){
                continue;
            }
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(pendingMessagesFileData);
            String fileContents = "";
            while(m.find()){
                fileContents = m.group(1);
            }
            String patternStatus = "<status>(.*?)</status>";
            Pattern p1 = Pattern.compile(patternStatus);
            Matcher m1 = p1.matcher(fileContents);
            
            String msgCountPattern = "<msgCount>(.*?)</msgCount>";
            Pattern p2 = Pattern.compile(msgCountPattern);
            Matcher m2 = p2.matcher(fileContents);
            
            pattern = "(<send>|<receive>)(.*?)(</send>|</receive>)";
            p = Pattern.compile(pattern);
            //p = Pattern.compile(pattern,Pattern.DOTALL);
            //We have to use DOTALL because the message has \n so the DOTALL tells java to consider all the line terminators in the dot!!!
            m = p.matcher(fileContents);

            int previousMessageCount = -1;
            int counter = 0;
            //The key is "s" for send and "r" for receive and value is the message!!!
            while(m.find()){
                counter++;
                String finalStatus = "0";
                String msgCount = "1";
                String statusOfUser = "";
                
                if(readUsers.contains(user)){
                    statusOfUser = "2";
                }else if(deliveredUsers.contains(user)){
                    statusOfUser = "1";
                }
                
                if(m1.find()){
                    finalStatus = m1.group(1);
                }
                if(m2.find()){
                    msgCount = m2.group(1);
                }
                int intMsgCount = Integer.parseInt(msgCount);
                if(counter != 1 && intMsgCount != (previousMessageCount+1)){
                    msgCount = (previousMessageCount+1)+"";
                    clientCode.getFileOperationsObj().setMsgCountinFile((previousMessageCount+1),user,clientPath);
                    previousMessageCount = previousMessageCount+1;
                }else{
                    previousMessageCount = intMsgCount;                     
                }
                if(statusOfUser!="" && (Integer.parseInt(statusOfUser) > Integer.parseInt(finalStatus)) && m.group(1).equals("<send>")){
                    clientCode.getFileOperationsObj().setStatusinFile(Integer.parseInt(statusOfUser),Integer.parseInt(msgCount) , user, clientPath);
                    finalStatus = statusOfUser;
                }

                int status = Integer.parseInt(finalStatus);

                if(m.group(1).equals("<send>")){
                    ContactComponent contactComponentObj = allContacts.get(user);                    
                    if(status == SENT_STATUS){
                        contactComponentObj.addPendingMessagePanel(new MessagePanel(new DESWorking().decrypt(clientCode.getFileOperationsObj().decodeSpecialTagsInMessage(m.group(2)))));//The above user send the message to the user whos dashboard is this!!!                        
                    }else if(status == DELIVERED_STATUS){
                        MessagePanel messagePanelObj = new MessagePanel(new DESWorking().decrypt(clientCode.getFileOperationsObj().decodeSpecialTagsInMessage(m.group(2))));
                        messagePanelObj.setStatus(DELIVERED_STATUS);
                        contactComponentObj.addPendingMessagePanel(messagePanelObj);//The above user send the message to the user whos dashboard is this!!!                        
                    }else if(status == READ_STATUS){
                        MessagePanel messagePanelObj = new MessagePanel(new DESWorking().decrypt(clientCode.getFileOperationsObj().decodeSpecialTagsInMessage(m.group(2))));
                        messagePanelObj.setStatus(READ_STATUS);
                        contactComponentObj.addPendingMessagePanel(messagePanelObj);//The above user send the message to the user whos dashboard is this!!!
                    }
                    new ClientSend(clientCode.getSocket(),username,"*",user,true,false,clientCode.getFileOperationsObj());//Message is Delivered!!!                    
                }else if(m.group(1).equals("<receive>")){
                    ContactComponent contactComponentObj = allContacts.get(user);
                    JLabel counterMsgLabel = contactComponentObj.getCounterMsgLabel();
                    if(status != READ_STATUS){
                        String counterMsgStr = counterMsgLabel.getText();
                        if(!"9+".equals(counterMsgStr)){
                            int tempInt = (Integer.parseInt(counterMsgStr) + 1);
                            if(tempInt > 9){
                                counterMsgLabel.setText("9+");
                            }else{
                                counterMsgLabel.setText(tempInt + "");                            
                            }
                        }
                        counterMsgLabel.setVisible(true);
                        contactComponentObj.getPanelCounter().setVisible(true);
                        contactComponentObj.updateUI();
                    }
                    ReceiveMessagePanel receiveMessagePanelObj = new ReceiveMessagePanel(new DESWorking().decrypt(clientCode.getFileOperationsObj().decodeSpecialTagsInMessage(m.group(2))));
                    receiveMessagePanelObj.setIdCounter(Integer.parseInt(msgCount));
                    contactComponentObj.addPendingMessagePanel(receiveMessagePanelObj);
                    new ClientSend(clientCode.getSocket(),username,"*",user,true,false,clientCode.getFileOperationsObj());//Message is Delivered!!!
                }
            }
        }
    }
    
    /**
     * This getter method is use to get the ContactComponent Panel object mapping to the username of receiver client of this dashboard!!!
     * @param username : This is string indicating the username of the receiver client of this dashboard.
     * @return : ContactComponent object extracted from the HashMap that we maintain.
     */
    public ContactComponent getContactComponentByUsername(String username){
        return allContacts.get(username);
    }
    
    /**
     * This method is use to add the new ContactComponent Panel into the Dashboard!!!
     * @param user : The String containing user name to be passed for creating ContactComponent Object.
     */
    public void addNewUserInContacts(String user){
        ContactComponent panel = new ContactComponent(user,username,clientCode);
        allContacts.put(user,panel);
        all_contact_panel.add(panel);
        all_contact_panel.updateUI();
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        all_contact_panel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        chat_inbox_header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        all_contact_panel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout all_contact_panelLayout = new javax.swing.GroupLayout(all_contact_panel);
        all_contact_panel.setLayout(all_contact_panelLayout);
        all_contact_panelLayout.setHorizontalGroup(
            all_contact_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        all_contact_panelLayout.setVerticalGroup(
            all_contact_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(all_contact_panel);

        jPanel2.setBackground(new java.awt.Color(20, 54, 99));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chatsystem/ui/images/User-White.png"))); // NOI18N

        chat_inbox_header.setFont(new java.awt.Font("Raleway SemiBold", 0, 24)); // NOI18N
        chat_inbox_header.setForeground(new java.awt.Color(255, 255, 255));
        chat_inbox_header.setText("Aryan's Chat Inbox");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chat_inbox_header)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(chat_inbox_header)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /** 
     * This is the situation in which the client goes offline (i.e when the user closes the window of Dashboard)!!!
     * So, we passed the request to Server to tell that this client is Log out and passed it to all other client's that this client is now offline!!!
    */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try{
            InetAddress address = InetAddress.getLocalHost();
            Socket logoutSocket = new Socket(address, 4000);
            //The getOutputStream() returns an output stream for writing bytes into this socket!!!
            PrintWriter output = new PrintWriter(logoutSocket.getOutputStream(), true);
            output.println(ClientConstants.LOG_OUT+":"+username);
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Issue : " + e.getMessage());
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel all_contact_panel;
    private javax.swing.JLabel chat_inbox_header;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
