/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations;

/**
 *
 * @author Aryan Dhami
 * This is a interface to store the constants for the server Side operations.
 */
public interface ServerConstants {
    String USERNAME_ERROR_CODE = "Username_Already_Exists";
    String ERROR_CODE = "Error";
    String SUCCESS_CODE = "Success";
    String ALREADY_LOGIN_CODE = "Already_Login";
    String LOG_OUT = "Logout";
}
