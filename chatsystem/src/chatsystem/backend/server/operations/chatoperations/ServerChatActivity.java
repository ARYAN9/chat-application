/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.chatoperations;

import chatsystem.db.MySQLConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentMap;


/**
 * This is the thread of server side which is responsible for the chat operations!!!
 * Whenever the client first time begins the chat by sending the his username to the server for the list of all the users then 
 * this hits the database and gets all the online users and offline users till now and passes it to ServerReceive!!!
 * And then ServerRecive internally starts the ServerReceive thread that will continuously listen to all the client messages!!!
 * @author Aryan Dhami
 */
public class ServerChatActivity implements Runnable{
    private Thread t;
    private String sqlQuery;
    private Connection conn;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private String allUsers;
    private ConcurrentMap<String,PrintWriter> clients;

    /**
     * This constructor establishes the connection with the database for retrieving the list of all the clients(i.e online plus offline) till now!!!
     * And it too starts a thread that will accept the socket and passed to ServerReceive thread for infinitely listening!!!
     * @param clients : This is the ConcurrentMap used here to provide the thread safe operation
     *                  because this map is being shared between login and chatActivity servers and also accessed simultaneously.
     *                  In this map the key is the username of the client and value is its socket stream!!!
     *                  We have directly saved the stream of the client so no need to create the object of same PrintWriter again for some operations!!!
     */
    public ServerChatActivity(ConcurrentMap<String,PrintWriter> clients) {
        t = new Thread(this);
        this.clients = clients;
        conn = MySQLConnect.getConnection();
        t.start();
    }
    
    /**
     * This method is hitting the database and getting the list of all the clients(i.e online plus offline) till now!!!
     * and embedding it into special format :- allOnlineUsers :: allOfflineUsers
     * where each online and offline user is separated by ":"
     * @return : String containing the list of all the online users till now from database!!!
     */
    private String getAllUsers(){
        try{
            /*To get all the online users*/
            sqlQuery = "SELECT username,status FROM user_list WHERE 1";
            ps = conn.prepareStatement(sqlQuery);
            rs = ps.executeQuery();
            
            String allOnlineUsers = "",allOfflineUsers = "",allUsers = "";
            //For sending all the usernames who are online we use ":" for separating them with each other because the ":" is not allowed in username!!!

            while(rs.next()){
                if(rs.getInt("status") == 0)
                    allOfflineUsers = allOfflineUsers + rs.getString("username") + ":";
                else if(rs.getInt("status") == 1)
                    allOnlineUsers = allOnlineUsers + rs.getString("username") + ":";                            
            }
            if(!"".equals(allOnlineUsers)){
                allOnlineUsers = allOnlineUsers.substring(0,allOnlineUsers.length()-1);//to remove the last ":"!!!                    
            }
            if(!"".equals(allOfflineUsers)){
                allOfflineUsers = allOfflineUsers.substring(0,allOfflineUsers.length()-1);//to remove the last ":"!!!                    
            }
            allUsers = allOnlineUsers + "::" + allOfflineUsers;
            /*
                If Person1,Person2 are online and Person3,Person4 are offline then value will be :-
                allUsers = "Person1:Person2::Person3:Person4";
            */
            return allUsers;//All the User online plus offline
        }catch(SQLException e){
            System.out.println("SQLException while reading registered users from db : " + e);
        }
        return "";
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */    
    @Override
    public void run(){
        try(ServerSocket serverSocket = new ServerSocket(4000)){
            while(true){
                Socket socket = serverSocket.accept();
                this.allUsers = getAllUsers();//Whenever a socket gets accepted at that time a current users (i.e online plus offline will get into this!!!)
                new ServerReceive(socket,allUsers,clients);
            }
        }catch(IOException e){
            System.out.println("ServerCode IOException : " + e.getMessage());
        }
    }
}
