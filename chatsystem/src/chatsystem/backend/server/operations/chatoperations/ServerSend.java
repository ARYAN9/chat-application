/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.chatoperations;

import java.io.PrintWriter;

/**
 * This thread is responsible for putting the message in the given client stream!!!
 * @author Aryan Dhami
 */
public class ServerSend implements Runnable{
    private Thread t;
    private String message;
    private PrintWriter printWriterObj;
    
    /**
     * This constructor just initializes the stream and message and starts the thread to put message in stream!!!
     * @param printWriterObj : The reference of the client stream which is stored in which message has to be put.
     * @param message : The String containing the actual message.
     */
    public ServerSend(PrintWriter printWriterObj, String message){
        this.message = message;
        this.printWriterObj = printWriterObj;
        t = new Thread(this);
        t.start();
    }
    public void run(){
        printWriterObj.println(message);
    }
}