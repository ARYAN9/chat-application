/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.chatoperations;

import chatsystem.backend.server.operations.ServerConstants;
import chatsystem.backend.helper.filehandling.FileOperations;
import chatsystem.db.MySQLConnect;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentMap;

/**
 * This is thread of serverSide that is responsible for :-
 *      - It will infinitely listen to all the messages of the client!!!
 *      - It first identifies what the client request is and it does that work corresponding to that request!!!
 *      - It also accepts all the messages of the client when the client is offline and accordingly stores them!!!
 *      - The messages store by this is delivered to the client when the client sends his first message to get the list of all users!!!
 *      - When the user close the Dashboard that is user is now offline it marks that user status as zero in database!!!
 *      - It sends the messages to all the clients when the new client comes online!!!
 *      - It also notifies all the online clients when one client goes offline!!!
 * @author Aryan Dhami
 */
public class ServerReceive implements Runnable,ServerConstants{
    private Socket socket;
    private ConcurrentMap<String, PrintWriter> clients;
    private Thread t;
    private String allUsers;
    private Connection conn = null;
    private PreparedStatement ps = null;
    
    /**
     * This constructor initializes the members and establishes the connection with database and starts a thread which will infinitely listen to the client!!!
     * @param socket : The socket of the client for saving it into Map for further operations!!!
     * @param allUsers : The list of all the users (i.e online plus offline) in special format :- allOnlineUsers :: allOfflineUsers
     * @param clients : The reference of the ConcurrentMap for saving client's socket!!!
     */
    public ServerReceive(Socket socket,String allUsers,ConcurrentMap<String,PrintWriter> clients) {
        this.socket = socket;
        this.clients = clients;
        this.allUsers = allUsers;
        conn = MySQLConnect.getConnection();
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
    */    
    @Override
    public void run(){
        readRequestsInfinitely();
    }
    
    /**
     * This infinitely reads the messages from the client and decodes and understand what the client means and process appropriately.
     * After processing the message received it sends to the ServerSend class so it writes in client stream.
     */
    private void readRequestsInfinitely(){
        try{
            String message;
            while(true){
                message = new BufferedReader(new InputStreamReader(socket.getInputStream())).readLine();
                
                if(message.indexOf(":") == 0 && message.indexOf("::") == -1){
                    String username = message.substring(1);
                    //This means the user is requesting server to give the list of pending messages which was delivered in his absence!!!
                    String pendingMessagesOfUser = FileOperations.readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\" + username + ".xml");
                    if(!"".equals(pendingMessagesOfUser)){
                        FileOperations.deleteFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\" + username + ".xml");
                    }
                    new ServerSend(clients.get(username),pendingMessagesOfUser);
                }else if(message.indexOf("::") == 0 && message.indexOf(":::") != 0 && message.indexOf("::::") != 0){//Here message = "::Aryan" sent by ClientCode.java
                    //This means that the client is first time requesting to the server for online list and putting his name as key in the HashMap!!!
                    String clientName = message.substring(2);
                    sendMessageToAll(clientName);
                    PrintWriter printWriterObj = new PrintWriter(socket.getOutputStream(), true);
                    clients.put(clientName,printWriterObj);
                    new ServerSend(printWriterObj,allUsers);
                }else if(message.indexOf(LOG_OUT) == 0 && message.charAt(6) == ':' && message.indexOf(":",7) == -1){
                    /*  
                        This means client wants to logout!!! For ex:- Logout:Aryan will be message!!!
                        This happens when the dashboard is closed!!!
                        So, now the client database status should be zero and client's PrintWriter object should also be removed from the HashMap!!!
                    */
                    String clientName = message.substring(7);
                    logoutTheUserFromDb(clientName);//set status = 0 means offline in Db!!!
                    clients.remove(clientName);//removed that client from the HashMap!!!
                    //Now broadcast this to all that this client is offline!!!
                    broadcastOfflineStatusOfClient(clientName);
                }else if(message.indexOf("::::") == 0){
                    //In case the message is been read!!!
                    //MessageFormat:- ::::senderName:Message:toClientName
                    String sender = message.substring(4,message.indexOf(":",4));
                    String toClientName = message.substring(message.lastIndexOf(":")+1);
                    String realMessage = message.substring(message.indexOf(":",4)+1,message.lastIndexOf(":"));
                    if(clients.containsKey(toClientName)){
                        new ServerSend(clients.get(toClientName),realMessage+sender);
                    }else{
                        //The client has been offline and its message has been read!!!
                        new FileOperations(toClientName, false).updateOfflineClient(sender,false);
                    }
                }else if(message.indexOf(":::") == 0){
                    //In case the message is been delivered!!!
                    //MessageFormat:- :::senderName:Message:toClientName
                    String sender = message.substring(3,message.indexOf(":",3));
                    String toClientName = message.substring(message.lastIndexOf(":")+1);
                    String realMessage = message.substring(message.indexOf(":",3)+1,message.lastIndexOf(":"));
                    if(clients.containsKey(toClientName)){
                        new ServerSend(clients.get(toClientName),realMessage+sender);
                    }else{
                        //The client has been offline and its message has been delivered!!!
                        new FileOperations(toClientName, false).updateOfflineClient(sender,true);
                    }
                }else{
                    //Here is the normal message passing between two clients through server!!!
                    String sender = message.substring(0,message.indexOf(":"));
                    String toClientName = message.substring(message.lastIndexOf(":")+1);
                    message = message.substring(message.indexOf(":")+1);
                    message = message.substring(0,message.lastIndexOf(":"));
                    /*
                        - We are checking containsKey because if the client is offline then NullPointerException will come if not check this 
                          because the socket is Null so PrintWriter is also null!!!
                        - So, null pointer exception avoided!!!
                    */
                    if(clients.containsKey(toClientName)){
                        //Message sending to the specified clientName!!!
                        new ServerSend(clients.get(toClientName),"%"+toClientName+":"+sender+"::"+message);
                    }else{
                        //This means that the client is offline and now the server has to save its messages!!!
                        //So, the Server will have the list of all the pending mesages of all the clients!!!
                        new FileOperations(toClientName,false).fillMessageInFileForReceive(sender,toClientName,message,0);
                    }
                }
            }
        }catch(IOException e){
            System.out.println("ServerReceive IOException : " + e.getMessage());
        }
    }
    
    /**
     * This method is responsible for broadcast the message to all the online clients that this client is online!!!
     * @param username : The String containing the username of the client which is to be broadcasted.
     */
    private void sendMessageToAll(String username){
        for(String clientName : clients.keySet()){
            new ServerSend(clients.get(clientName), "@"+username);//"@" is for online!!!
        }
    }

    /**
     * This method is responsible for broadcast the message to all the online clients that this client is offline!!!
     * @param username : The String containing the username of the client which is to be broadcasted.
     */    
    private void broadcastOfflineStatusOfClient(String username){
        for(String clientName : clients.keySet()){
            new ServerSend(clients.get(clientName), "#"+username);//"#" is for offline!!!
        }
    }
    
    /**
     * This method is call when the client closes the Dashboard means the client is offline!!!
     * So, this method updates the status column of the client to zero indicating the offline status!!!
     * @param username : String indicating the username of the client who is offline.
     */
    private void logoutTheUserFromDb(String username){
        try {
            String sqlQuery = "UPDATE user_list SET status = 0 WHERE username = ?";
            ps = conn.prepareStatement(sqlQuery);
            ps.setString(1, username);
            ps.execute();
        }catch(SQLException e){
            System.out.println("ServerReceive SQLException while updating : " + e.getMessage());
        }
    }
}