/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.loginoperations;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This thread is use to send the message (i.e the response of the server on the login or register) to client!!!
 * @author Aryan Dhami
 */
public class ServerLoginWrite implements Runnable{
    private Thread t;
    private Socket socket;
    private String status = "";

    /**
     * This constructor just initializes socket of client and status of server on login or register 
     * and starts a thread to push that message onto client stream!!!
     * @param socket : The socket of the client.
     * @param status : The status of server.
     */
    public ServerLoginWrite(Socket socket, String status){
        this.socket = socket;
        this.status = status;
        
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */
    @Override
    public void run(){
        try(PrintWriter output = new PrintWriter(socket.getOutputStream(), true)){
            output.println(status);
        }catch(IOException e){
            System.out.println("ServerLoginWrite IOException : " + e.getMessage());
        }
    }
}
