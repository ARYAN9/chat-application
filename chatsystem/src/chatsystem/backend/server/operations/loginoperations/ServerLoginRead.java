/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.loginoperations;

import chatsystem.db.MySQLConnect;
import chatsystem.backend.server.operations.ServerConstants;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentMap;

/**
 * This thread is responsible for responding the status of client on getting the username and password from the client request for either login or register!!!
 * @author Aryan Dhami
 */
public class ServerLoginRead implements Runnable,ServerConstants{
    private Thread t;
    private Socket socket;
    private Connection conn;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private ConcurrentMap<String,PrintWriter> clients;
    
    /**
     * This constructor just initializes socket and Map and establishes the connection with database!!!
     * @param socket : The socket of the client to which this thread listens.
     * @param clients : The clients ConcurrentMap is passed so when the new client gets registered,
     *                  this class with the help of this ConcurrentMap clients will notify all the existing clients that this new client is registered!!!
     */
    public ServerLoginRead(Socket socket,ConcurrentMap<String,PrintWriter> clients) {
        this.socket = socket;
        this.clients = clients;
        conn = MySQLConnect.getConnection();
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */
    @Override
    public void run(){
        try{
            Scanner scanner = new Scanner(new InputStreamReader(socket.getInputStream()));
            scanner.useDelimiter(":");
            //Here we get the operation which is either the "login" or "register"
            String operation = scanner.next();
            scanner.skip(scanner.delimiter());
            String username = scanner.next();
            scanner.skip(scanner.delimiter());
            String password = scanner.nextLine();//We can use nextLine() because this is the last element!!!(So read till end of the line!!!)
            
            if(operation.equalsIgnoreCase("login")){
                new ServerLoginWrite(socket, check(username, password));
            }else if(operation.equalsIgnoreCase("register")){
                new ServerLoginWrite(socket, insert(username, password));
            }
        }catch(IOException e){
            System.out.println("ServerLoginRead IOException e : " + e.getMessage());
        }
    }
    
    /**
     * This method will be call if the client is requesting to login!!!
     * This method will first check that is client is existing by checking that client record in database 
     * and if the client is there it will check whether the username and password entered by client on UI is matching to client's database username password!!! 
     * @param username : The username of the client.
     * @param password : The password of the client.
     * @return : String containing the constant value of the status.(i.e which is shared between client and server(protocols))
     */
    private String check(String username, String password){
        try{
            /*
                - If the status is 1 the user is online
                - If the status is 0 the user is offline
                Here we are checking the query which will return that recored whose username and password is as that entered by user and
                status is zero(i.e it is offline)
                The status zero is checked to ensure the same user does not logged in twice!!!
            */
            //SELECT * FROM `user_list` where BINARY username = 'aryan' AND password = "19821"
            String sqlQuery = "SELECT status FROM user_list WHERE BINARY username = ? AND password = ?";
            /*
                Here we have to put the Binary keyword for the case sensitive comparision!!!
                Because the on a byte-by-byte basis, "aryan" and "Aryan" they are NOT equivalent!!!
                While on the character-by-character basis they are equal!!!
                So, by this the username will be case sensitive!!!
            */
            ps = conn.prepareStatement(sqlQuery);
            ps.setString(1, username);
            ps.setString(2, password);
            
            rs = ps.executeQuery();
            if(rs.next()){
                int status = rs.getInt("status");
                if(status == 1){
                    //Here it is ensure that the user have already login and again trying to login!!!
                    return ALREADY_LOGIN_CODE;
                }else{
                    /*Updating the current user status as 1*/
                    sqlQuery = "UPDATE user_list SET status = 1 WHERE username = ?";
                    ps = conn.prepareStatement(sqlQuery);
                    ps.setString(1, username);
                    ps.execute();
                    return SUCCESS_CODE;//All the User online plus offline
                }
            }
        }catch(SQLException e){
            System.out.println("ServerLoginRead SQLException while checking: " + e.getMessage());
        }
        return ERROR_CODE;
    }
        
    /**
     * This method is call when the client request is Register.
     * And this responsible for :-
     *      1. It checks whether the username client wants to register is already there, if there already then client cannot register.
     *      2. After finding the username is not register already it inserts a new record in database with the username and password of this client.
     * @param username : The username of the client.
     * @param password : The password of the client.
     * @return : String containing the constant value of the status.(i.e which is shared between client and server(protocols))
     */
    private String insert(String username, String password){
        try{
            String sqlQuery = "SELECT * FROM user_list WHERE username = ?";
            ps = conn.prepareStatement(sqlQuery);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if(rs.next()){
                return USERNAME_ERROR_CODE;
            }else{
                sqlQuery = "INSERT INTO user_list(username,password, status) VALUES (?,?,0)";
                ps = conn.prepareStatement(sqlQuery);
                ps.setString(1, username);
                ps.setString(2, password);
                ps.execute();
//                JOptionPane.showMessageDialog(null,"You have been successfully registered!!!");
                //Now broadcast all the users that this user is registered!!!
                for(String user : clients.keySet()){
                    clients.get(user).println("$"+username);
                }
                return SUCCESS_CODE;
            }                        
        }catch(SQLException e){
            System.out.println("ServerLoginRead SQLException while inserting : " + e.getMessage());
        }
        return ERROR_CODE;
    }
}
