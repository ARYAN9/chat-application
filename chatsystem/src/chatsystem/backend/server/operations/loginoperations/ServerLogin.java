/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations.loginoperations;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;

/**
 * This is the thread to which the client requests server when the client has to login or register and 
 * so this infinitely listens to the port where the client will request on login or register !!!
 * And then it passes it to ServerLoginRead thread which do authentication and similar work of login!!!
 * And thereby reducing the load so, at that time this can listen to another clients while ServerLoginRead will do work of previous clients!!!
 * @author Aryan Dhami
 */
public class ServerLogin implements Runnable{
    private Thread t;
    private ConcurrentMap<String,PrintWriter> clients;

    /**
     * The ConcurrentMap shared by the main server is just initialize in this constructor.
     * @param clients : This is the ConcurrentMap used here to provide the thread safe operation 
     *                  because this map is being shared between login and chatActivity servers and also accessed simultaneously.
     */
    public ServerLogin(ConcurrentMap<String,PrintWriter> clients) {
        this.clients = clients;
        t = new Thread(this);
        t.start();
    }

    /**
     * Just overriding the method of Runnable Interface!!!
     */    
    @Override
    public void run(){
        try(ServerSocket loginSocket = new ServerSocket(3000)){
            while(true){
                Socket socket = loginSocket.accept();
                new ServerLoginRead(socket,clients);
            }
        }catch(IOException e){
            System.out.println("ServerLogin IOException : " + e.getMessage());
        }
    }
}
