/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.server.operations;

import chatsystem.backend.server.operations.chatoperations.ServerChatActivity;
import chatsystem.backend.server.operations.loginoperations.ServerLogin;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * This is the class which handles the Server Side Operations with the help other thread classes!!!
 * This main server starts the two servers which handle two things :-
 *      1. The Login Server to handle the Login Operations.
 *      2. The ServerChatActivity that handles all the chat operations work between many clients.
 * Most importantly it is used to connect the server modules and accept the Client connection infinitely by starting the above two servers!!! 
 * @author Aryan Dhami
 */
public class ServerCode {
    private static ConcurrentMap<String,PrintWriter> clients;
    /**
     * This is done to share the HashMap because while the new client is registered the login server also needs to access this HashMap for this one time only!!!
     * So, by passing this to both we can share same Map between both of them!!!
     * This is the ConcurrentMap used here to provide the thread safe operation 
     * because this map is being shared between login and chatActivity servers and also accessed simultaneously.
    */
    public static void main(String[] args) {
        /**
            - Here the two servers are created which are actively accepting the requests on different ports!!!
              (i.e LoginServer on port 3000 and ChatServer on port 4000)
            - So, anyone who is trying to login will be directed to Login Server and 
              anyone who is chatting will be directed to Chat Server!!!
        */
        clients = new ConcurrentHashMap<>();
        new ServerLogin(clients);
        new ServerChatActivity(clients);
    } 
}