/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.client.operations;

import chatsystem.backend.helper.filehandling.FileOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This thread sends the client message to the Server by encoding that message in some special format!!!
 * That message can be normal message or to notify another client through server that the message is delivered or read!!!
 * @author Aryan Dhami
 */
public class ClientSend implements Runnable{
    private Thread t;
    private Socket socket;
    private String message;
    private String sender;
    private String toClientName;
    private boolean isDelivered = false;
    private boolean isRead = false;
    private FileOperations fileOperationsObj;
    
    /**
     * The below constructor is specially for normal message!!!
     * @param socket : Socket of the client.
     * @param sender : Sender is basically the name of the client.
     * @param message : The message stores the actual message.
     * @param toClientName : The another client name to which this message has to be send through server.
     * @param fileOperationsObj : The FileOperations class reference object for File Handling.
     */
    public ClientSend(Socket socket, String sender, String message, String toClientName,FileOperations fileOperationsObj){
        this.socket = socket;
        this.sender = sender;
        this.message = message;
        this.toClientName = toClientName;
        this.fileOperationsObj = fileOperationsObj;
        
        t = new Thread(this);
        t.start();
    }
    
    /**
     * The below constructor is specially for delivered and read message status of client!!! 
     * @param socket : Socket of the client.
     * @param sender : Sender is basically the name of the client.
     * @param message : The message stores the actual message.
     * @param toClientName : The another client name to which this message has to be send through server.
     * @param isDelivered : If this is true than the status of the message is delivered.
     * @param isRead : If this is true than the status of the message is read.
     * @param fileOperationsObj : The FileOperations class reference object for File Handling.
     */
    public ClientSend(Socket socket, String sender, String message, String toClientName,boolean isDelivered,boolean isRead,FileOperations fileOperationsObj){
        this.socket = socket;
        this.sender = sender;
        this.message = message;
        this.toClientName = toClientName;
        this.isDelivered = isDelivered;
        this.isRead = isRead;
        this.fileOperationsObj = fileOperationsObj;
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */
    @Override
    public void run(){
        try{
            String messageFormat = "";
            
            //We can only use ":" because only ":" is not allowed in all the usernames!!!
            if(isDelivered){
                messageFormat = ":::"+sender + ":" + message + ":" + toClientName;
            }
            else if(isRead){
                messageFormat = "::::" + sender + ":" + message + ":" + toClientName;
            }
            else{
                messageFormat = sender + ":" + message + ":" + toClientName;                
                fileOperationsObj.fillMessageInFileForSend(sender, toClientName, message, 0);
            }
            
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println(messageFormat);
        }catch(IOException e){
            System.out.println("ClientSend IOException : " + e.getMessage());
        }
    }
}
