/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.client.operations;

import chatsystem.backend.helper.filehandling.FileOperations;
import chatsystem.ui.Dashboard;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
/**
 * This thread is responsible for:-
 *  - For requesting the server to save the socket of the client in the server data!!!
 *  - To request the Server to give the list of all the online users to be displayed on the client UI!!!
 *  - To get the Pending Messages of the client from the Server which were delivered to client when the client was offline!!!
 *  - To invoke the ClientReceive Thread to continuously listen the messages that comes to this client from the server!!! 
 *
 * @author Aryan Dhami
 */
public class ClientCode implements Runnable{
    private String username;
    private Dashboard dashboardObj;
    private Thread t;
    private Socket socket;
    private ClientReceive clientReceive;
    private FileOperations fileOperationsObj;
    
    /**
     *                           CONSTRUCTOR
     * This constructor just basically initializes name and dashboard reference object and 
     * it also starts the thread to continuously listen the messages that comes to this client from the server.
     *  
     * @param username : This is the name of the client.
     * @param dashboardObj : This is the reference object of the Dashboard class(To show the online users on UI).
     */
    public ClientCode(String username,Dashboard dashboardObj) {
        this.username = username;
        this.dashboardObj = dashboardObj;
        this.fileOperationsObj = new FileOperations(username,true);
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */
    public void run(){
        try{
            socket = new Socket("localhost", 4000);
            //Sending the message to the server to request the list of online users by sending my username to save my socket!!!
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            output.println("::"+username);//"::Aryan" means that Aryan is online to server!!!
            //Message Format :- Aryan:Mihir::sjdhsj

            //The first message received from server will always be a list of registered users!!!
            //Here we get the list of all the online users in the String colon separated!!!
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String allUsers = input.readLine();
            
            output.println(":"+username);
            String pendingMessages = input.readLine();
                        
            dashboardObj.customInit(allUsers,pendingMessages);
            //One dashboard equals one ClientCode equals one ClientReceive
            clientReceive = new ClientReceive(socket, dashboardObj,username,fileOperationsObj);
            while(true){}

        }catch(IOException e){
            System.out.println("IOException in Client Code : " + e);
        }
    }
    
    /**
     * GETTERS to get the Socket and FileOperations class reference!!!
     */
    
    public Socket getSocket(){
        return this.socket;
    }
    
    public FileOperations getFileOperationsObj(){
        return fileOperationsObj;
    }
}
