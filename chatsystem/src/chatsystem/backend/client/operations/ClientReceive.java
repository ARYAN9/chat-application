/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.client.operations;

import chatsystem.backend.client.clientconstants.ClientConstants;
import chatsystem.backend.client.cryptography.desoperations.DESWorking;
import chatsystem.backend.helper.filehandling.FileOperations;
import chatsystem.ui.ChatWindow;
import chatsystem.ui.ContactComponent;
import chatsystem.ui.Dashboard;
import chatsystem.ui.MessagePanel;
import chatsystem.ui.ReceiveMessagePanel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * This is the thread that handles all the responses from the server for this client!!!
 * It continuously listen the messages that comes to this client from the server through the stream shared by the client and the server!!!
 * And as soon as the response is received from server it processes it!!!
 * @author Aryan Dhami
 */
public class ClientReceive implements Runnable,ClientConstants{
    private Thread t;
    private Socket socket;
    private Dashboard dashboardObj;
    private String username;
    private FileOperations fileOperationsObj;

    /**
     * This constructor just initializes all the variables.
     * @param socket : The socket of the client for continuously listening the stream of client!!! 
     * @param dashboardObj : The the reference of the Dashboard class for UI handling!!!
     * @param username : The username of the client!!!
     * @param fileOperationsObj : The FileOperations helper class reference object created once for client is passed for File Handling work!!!
     */
    public ClientReceive(Socket socket, Dashboard dashboardObj,String username,FileOperations fileOperationsObj) {
        this.socket = socket;
        this.dashboardObj = dashboardObj;
        this.username = username;
        this.fileOperationsObj = fileOperationsObj;
        t = new Thread(this);
        t.start();
    }
    
    /**
     * Just overriding the method of Runnable Interface!!!
     */
    @Override
    public void run(){
        readResponsesContinuously();
    }
    
    /**
     * This method infinitely reads the messages from the server and 
     * then identifies the type of response that the server want to tell and 
     * then processes the messages accordingly!!!
     * This method before processing the messages also decrypt the messages that was encrypt by the client who has send the message through server!!!
     */
    private void readResponsesContinuously() {
        try{
            while(true){
                String message = new BufferedReader(new InputStreamReader(socket.getInputStream())).readLine();
                //@ for online and # for offline and $ for newly registered user!!!

                String startingCharOfMessage = message.charAt(0)+"";
                if("@".equals(startingCharOfMessage)){
                    //This means rest of the message is the name of the client which is online!!!
                    if(!message.substring(1).equals(username)){
                        ContactComponent obj = dashboardObj.getContactComponentByUsername(message.substring(1));
                        if(obj != null){
                            obj.setContact_latest_msg("Online");
                        }                    
                    }
                }else if("#".equals(startingCharOfMessage)){
                    //This means rest of the message is the name of the client which is offline!!! 
                    ContactComponent obj = dashboardObj.getContactComponentByUsername(message.substring(1));
                    if(obj != null){
                        obj.setContact_latest_msg("Offline");
                    }
                }else if("$".equals(startingCharOfMessage)){
                    //This means rest of the message is the name of the client which is newly registered!!! 
                    dashboardObj.addNewUserInContacts(message.substring(1));
                }else if("%".equals(startingCharOfMessage)){
                    //Normal message is there!!!
                    //Message Format is :- %receiverName:senderName::Message
                    
                    String receiverName = message.substring(1,message.indexOf(":"));
                    String senderName = message.substring(message.indexOf(":")+1,message.lastIndexOf("::"));
                    String realMesssage = message.substring(message.indexOf("::")+2);
                    /*     -File part
                           -Because by default the status of any message is sent so zero in last parameter!!!
                           -In file only put encrypted message which is already encrypted on the send button!!!
                    */
                    fileOperationsObj.fillMessageInFileForReceive(senderName,receiverName,realMesssage,0);
                    
                    //We are now decrypting the message to show on the UI!!!
                    realMesssage = new DESWorking().decrypt(realMesssage);

                    ContactComponent contactComponentObj = dashboardObj.getContactComponentByUsername(senderName);
                    ChatWindow chatWindowObj = contactComponentObj.getChatWindowObj();
                                        
                    boolean flag = false;
                    if(!contactComponentObj.isChatWindowOn){
                        flag = true;
                        contactComponentObj.getCounterMsgLabel().setVisible(true);
                        contactComponentObj.getPanelCounter().setVisible(true);
                        String tempString = contactComponentObj.getCounter_msg_label();
                        if(!tempString.equals("9+")){
                            int latestCount = Integer.parseInt(tempString);
                            latestCount = latestCount+1;
                            if(latestCount > 9)
                                contactComponentObj.setCounter_msg_label("9+");                            
                            else
                                contactComponentObj.setCounter_msg_label(latestCount + "");
                        }
                        
                        new ClientSend(socket,receiverName,"*",senderName,true,false,fileOperationsObj);//Message is Delivered!!!
                        // "*" :- Star indicates the message has been delivered!!!
                    }else{
                        //Means the ChatWindow of this ContactComponent is on!!!
                        new ClientSend(socket,receiverName,"&",senderName,false,true,fileOperationsObj);//Message is Read!!!
                        // "&" :- And indicates the message has been read!!!
                    }
                    /*
                        If the chatWindowObj is null then the user has not until now clicked on the ContactComponent!!!
                    */
                    
                    if(chatWindowObj!=null){
                        chatWindowObj.setMessageReceive(realMesssage);                        
                    }else{
                        //Here we come user has not until now clicked on the ContactComponent!!!
                        //So, we have to now stored it into the ContactComponent Pendind Messages list!!!
                        ReceiveMessagePanel receiveMessagePanelObj = new ReceiveMessagePanel(realMesssage);
                        int latestCounter = fileOperationsObj.getLatestCount(fileOperationsObj.readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+receiverName+".xml"), senderName);

                        receiveMessagePanelObj.setIdCounter(latestCounter);
                        contactComponentObj.addPendingMessagePanel(receiveMessagePanelObj);  
                        if(!flag){
                            contactComponentObj.getCounterMsgLabel().setVisible(true);
                            contactComponentObj.getPanelCounter().setVisible(true);
                            
                            String tempString = contactComponentObj.getCounter_msg_label();
                            if(!tempString.equals("9+")){
                                int latestCount = Integer.parseInt(tempString);
                                latestCount = latestCount+1;
                                if(latestCount > 9)
                                    contactComponentObj.setCounter_msg_label("9+");                            
                                else
                                    contactComponentObj.setCounter_msg_label(latestCount + "");                                
                            }
                        }
                    }
                }else if("*".equals(startingCharOfMessage)){
                    String senderName = message.substring(1);
                    ContactComponent contactComponentObj = dashboardObj.getContactComponentByUsername(senderName);
                    ChatWindow chatWindowObj = contactComponentObj.getChatWindowObj();
                    /*
                        - if we donot put this in the synchronized block then the ArrayList will get compromized!!!
                        - java.util.ConcurrentModificationException will occur when this messagePanels ArrayList is concurrently accessed here 
                          and in ChatWindow class!!!
                    */
                    if(chatWindowObj != null){//To avoid NullPointerException!!!
                        synchronized(chatWindowObj.messagePanels){
                            for(MessagePanel messagePanelObj : chatWindowObj.messagePanels){
                                if(messagePanelObj.getStatus() == SENT_STATUS){
    //                                try{
    //                                    Thread.sleep(1000);//Just for the time the single tick to double tick seen by the user!!!              
    //                                }catch(InterruptedException ie){
    //                                    System.out.println("Error is sleeping thread:"+ie.getMessage());
    //                                }

                                    messagePanelObj.setStatus(DELIVERED_STATUS);
                                    fileOperationsObj.setStatusinFile(DELIVERED_STATUS, messagePanelObj.getIdCounter(), senderName,"C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+username+".xml");
                                }
                            }                        
                        }
                    }
                }else if("&".equals(startingCharOfMessage)){
                    String senderName = message.substring(1);
                    ContactComponent contactComponentObj = dashboardObj.getContactComponentByUsername(senderName);
                    ChatWindow chatWindowObj = contactComponentObj.getChatWindowObj();
                    if(chatWindowObj!=null){
                        synchronized(chatWindowObj.messagePanels){
                            for(MessagePanel messagePanelObj : chatWindowObj.messagePanels){
                                messagePanelObj.setStatus(READ_STATUS);
                                fileOperationsObj.setStatusinFile(READ_STATUS,messagePanelObj.getIdCounter(),senderName,"C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+username+".xml");
                            }
                        }
                    }
                }
            }
        }catch(IOException e){
            System.out.println("Client Receive Exception : " + e.getMessage());
        }
    }
}
