/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.client.clientconstants;

/**
 *
 * @author Aryan Dhami
 * This is a interface to store the constants for the client Side operations.
 */
public interface ClientConstants {
    String USERNAME_ERROR_CODE = "Username_Already_Exists";
    String ERROR_CODE = "Error";
    String SUCCESS_CODE = "Success";
    String ALREADY_LOGIN_CODE = "Already_Login";    
    String LOG_OUT = "Logout";
    int SENT_STATUS = 0;
    int DELIVERED_STATUS = 1;
    int READ_STATUS = 2;
}
