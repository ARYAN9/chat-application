package chatsystem.backend.client.cryptography;
public interface Cryptography {
    String encrypt(String message);
    String decrypt(String message);
}