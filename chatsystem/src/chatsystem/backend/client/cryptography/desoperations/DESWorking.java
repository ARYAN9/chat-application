package chatsystem.backend.client.cryptography.desoperations;

import chatsystem.backend.client.cryptography.Cryptography;
import chatsystem.backend.client.cryptography.desconstants.CipherSpecification;
import java.util.*;

//Entire Working operation of the DES goes under this class!!!
public class DESWorking implements Cryptography{
    private static String k[] = new String[16];
    public static String finalStringArray[];
    private static final int numberOfCores = Runtime.getRuntime().availableProcessors();//To get number of cores in system!!!
    private static CreateThreads t[] = new CreateThreads[numberOfCores];//This is the array of the Threads!!! 
    private static int finalStringArrayIndexNo,numOfThreads;
    private static String finalString,message;
    private static int startIndex, endIndex;
    Scanner scan = new Scanner(System.in);  

    /**
    *This method is used to encrypt the plain text to cipher text.
    *@return String This returns the encrypted cipher text.
    */
    public String encrypt(String message){
        this.startIndex = numberOfCores*16;
        this.endIndex = startIndex+16;
        this.message = message;
        this.message = convertStringToHex(this.message);
        this.message = convertMessageIntoFormat(this.message);
        String encryptedString = executeDES(this.message,true);
        System.out.println("encryptedString:"+encryptedString);
        return encryptedString;
    }//end of the encrypt method !!!+

    /**
    *This method is used to decrypt the cipher text to plain text.
    *@return String This returns the decrypted cipher text.
    */
    public String decrypt(String message){
        this.startIndex = numberOfCores*16;
        this.endIndex = startIndex+16;
        this.message = message;
        String message_converted = convertMessageIntoFormat(this.message);
        this.message = message_converted;
        String decryptedStringWith0D0A = executeDES(this.message,false);
        String decryptedString = remove0D0A(decryptedStringWith0D0A);
        return convertHexToString(decryptedString);
    }//end of the decrypt method !!!

    /**
    *This method is use to remove the "0d0a" which was added at the time of the encryption to indicate the end!!!
    *Thereby we are assuring that after finding the "0d0a" all the other bits in the message are zero than discard it simply!!!
    *@param String: The decrypted string which has to be given for discarding!!!
    *@return String : The final string after discarding the "0d0a" and further zeros!!!
    */
    private String remove0D0A(String decryptString){
        int index = decryptString.lastIndexOf("0d0a");
        int i;
        if(index != -1){
            for(i = index+4;i<decryptString.length();i++){
                if(decryptString.charAt(i) != '0'){
                    break;
                }
            }
            if(i == decryptString.length()){
                return decryptString.substring(0,index);
            }
        }
        return decryptString;
    }

    /**
    *This method converts the String into the format that is divisible by 16
    *@param String : It is the string message that is to be converted!!!
    *@return String : Final String after converting it into proper format!!!
    */
    private String convertMessageIntoFormat(String message){
        int n = message.length()/16;

        if(n != 0){
            if(!(isMultipleOf16(message))){
                String convertedPartOfmessage = convertInto16Bits(message.substring(((n-1)*16),message.length()));
                String newMessage = "";
                for(int k = 0; k < ((n-1)*16); k++)
                    newMessage += message.charAt(k); 
                newMessage += convertedPartOfmessage;
                message = newMessage;
            }
        }
        else{
            if(message.length() != 16){
                message = convertInto16Bits(message);
           }
        }
        return message;
    }

    /**
    * This function does the entire working of the DES algo!!!
    * @param String : This string contains the message that needs to be encrypted!!!
    * @param boolean : This contains the boolean if it is true than it is encryption else it is decryption(ie false)
    * @return String : Returns the final String after encryption or decryption!!!
    */
    private String executeDES(String message,boolean isEncrypt){
        if(message.length() == 16){ 
            //Step 1
            String messageInBits = convertIntoBits(message,true);
            String keyInBits = convertIntoBits(CipherSpecification.key,true);
            String permutedKey = permutationOfMatrix(CipherSpecification.pc1,keyInBits);
            String c0 = permutedKey.substring(0,28);
            String d0 = permutedKey.substring(28,56);
            String cd[] = initailizeC1ToC16AndD1ToD16(c0,d0);
            initailizeK1ToK16(cd);

            //Step2 Begins!!!(i.e Encoding the 64 bit block of the data)
            String messageIntoIP = permutationOfMatrix(CipherSpecification.ip,messageInBits);
            String l0 = messageIntoIP.substring(0,32);
            String r0 = messageIntoIP.substring(32,64);
            String finalString;
            if(isEncrypt == true)
                finalString = initailizeL16AndR16(l0,r0,false);
            else
                finalString = initailizeL16AndR16(l0,r0,true);
            return finalString;
        }
        else{
            String keyInBits = DESWorking.convertIntoBits(CipherSpecification.key,true);
            String permutedKey = DESWorking.permutationOfMatrix(CipherSpecification.pc1,keyInBits);    
            String c0 = permutedKey.substring(0,28);
            String d0 = permutedKey.substring(28,56);
            String cd[] = DESWorking.initailizeC1ToC16AndD1ToD16(c0,d0);
            initailizeK1ToK16(cd);
    
            //run many times!!!
            finalStringArray = new String[(message.length()/16)];
            numOfThreads = (message.length()/16);
            finalStringArrayIndexNo = 0;
            if(numOfThreads > numberOfCores){
                int i,j,k;
                for(i=0,j=0,k=16;i<numberOfCores;i++,finalStringArrayIndexNo++,j+=16,k+=16){
                    if(isEncrypt == true)
                        t[i] = new CreateThreads(finalStringArrayIndexNo,message.substring(j,k),i,false,true);
                    else
                        t[i] = new CreateThreads(finalStringArrayIndexNo,message.substring(j,k),i,true,true);
                }
                numOfThreads -= numberOfCores;

                for(i=0;i<numberOfCores;i++)  
                   while(t[i].alive){;}

                finalString = "";
                for(i=0;i<finalStringArray.length;i++){
                    finalString += finalStringArray[i];
                    // System.out.println("i is:"+i+" and finalStringArray[i] is:"+finalStringArray[i]);
                }
                return finalString;       
            }else{
                CreateThreads t[] = new CreateThreads[numOfThreads];//This is the array of the Threads!!!
                for(int i=0,j=0,k=16;i<numOfThreads;i++,j+=16,k+=16){
                    if(isEncrypt == true)
                        t[i] = new CreateThreads(i,message.substring(j,k),i,false,false);
                    else   
                        t[i] = new CreateThreads(i,message.substring(j,k),i,true,false);   
                }
                for(int i=0;i<numOfThreads;i++)                        
                    while(t[i].t.isAlive()){;} 

                String finalString = "";
                for(int i=0;i<numOfThreads;i++){
                    finalString += finalStringArray[i];
                }
                return finalString;
           }
        }
    }//end of the executeDES method!!!
 
    /**
    *To understand this first understand logic:-
    *This method will be called when there are more than numOfCore threads (i.e the message.length()/16 is more than numOfCore)
    *And now logic is:-
    *Firstly, numOfCore threads will be started and then we will see that the thread who finishes first amoung this all threads will be again reassign the work which has to be done by the next thread!!!
    ***Thus, this ensures at any time there will be numOfCore threads running in the system!!!
    *
    * And this method is made synchronized because the startIndex and endIndex are shared between all the Threads!!!
    * And if we not make it synchronized then at a time two or more threads can access it simultaneously where one is incremented and other is not !!!
    *** Thus, by making it synchronized we ensure that at a time only one Thread will enter in this!!!
    *
    *@param int : This int value is indicating what is the number of the thread amoung five threads!!!
    *@param boolean : This boolean indicates whether it is encrypt or decrypt!!!
    *@param CreateThreads :This object is passed to it to set the alive flag (member of this object) to false inorder to stop the execution of the thread!!!
    */
    static synchronized void reassigningTheThread(int threadArrayIndexNo,boolean isReverse,CreateThreads obj){              
        if(startIndex < message.length() && endIndex <= message.length()){
            if(numOfThreads != 0){
                t[threadArrayIndexNo] = new CreateThreads(finalStringArrayIndexNo++,message.substring(startIndex,endIndex),threadArrayIndexNo,isReverse,true);
                startIndex+=16;
                endIndex+=16;
                numOfThreads-=1;
            }    
        }else{
            System.out.println("The number of thread completed is:"+threadArrayIndexNo);
            //among the five threads this the thread who have this thread number is completed!!!
            obj.setAlive(false);
        }
    }
    /**
    *This method is used to convert the given String input to the equivalent 16 bit String by padding zeros.
    *To indicate the padding starts the "0D0A" is added at the starting of the string.
    *@param input It is the input string to the method to convert.
    *@return String This returns the padded 16 bit String.
    */
    String convertInto16Bits(String input){
        input += "0D0A"; // adding 0A0D in input indicating the end!!!
        while(!(isMultipleOf16(input))){
            input = input + "0";
        }
        return input;        
    }
    
    /**
    *This method is used to check the given string is multiple of 16 or not.
    *@param input It is the input string given to method to check.
    *@return boolean This returns the true if given string is multiple of 16 else false.
    */
    boolean isMultipleOf16(String input){
        if((input.length() % 16) == 0){
            return true;
        }else{
            return false;
        }
    }
    //This method will take the character as parameter and return the coressponding binary value in String!!!
    
    /**
    *This method is used to convert the number(i.e character) to the equivalent binary. 
    *@param number It is the input number in the form of character to the method. 
    *@return String It returns corresponding Binary String.
    */
    static String numberToBit(char number){
        return Integer.toBinaryString(Character.getNumericValue(number));
    }

    //It is the same overLoaded mehod which takes String as an argument!!!
    static String numberToBit(String number){
        return Integer.toBinaryString(Integer.parseInt(number));
    }

    /** 
    *This method will convert the given input String to equivalent Binary in four bits!!!
    *@param String: It is input string to be converted!!!
    *@param boolean : Here in the below method if the flag is true then it will check each character in the string and compute the binary 
    *else if the flag is false then the Whole String is directly computed and given the binary!!!
    */
    static String convertIntoBits(String input,boolean flag){
        String check = "",messageInBits = "";
        if(flag == true){
            for(int i=0;i<input.length();i++){
                check = numberToBit((input.charAt(i))); 
                if(check.length() < 4)//If the Length is not 4 than padd it with the Dummy Zeros!!!
                {
                    while(check.length() != 4){
                        check = "0" + check;
                    }
                }
                messageInBits += check;
            }
            check = messageInBits;
        }else{
            check = numberToBit(input);
            if(check.length() < 4)//If the Length is not 4 than padd it with the Dummy Zeros!!!
            {
                while(check.length() != 4){
                    check = "0" + check;
                }
            }
        }
        return check;   
    }   
    
     /**
     * The below method would accept a 2D integer array and an input string. It would then 
     * generate the output string depending on the permutationTable. 
     * This method will work for all permutation task required.
     */

    static String permutationOfMatrix(int matrix[][],String computationString){
        String permutedKey = "";
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                permutedKey += (computationString.charAt(matrix[i][j]-1));  
            }
        }
        return permutedKey;
    }
    //c[0] = C1!!!
    static String[] initailizeC1ToC16AndD1ToD16(String c0,String d0){
        String c[] = new String[17];
        String d[] = new String[17];
        c[0] = c0;
        d[0] = d0;
        for(int i=1,k=0;i<17;i++,k++){
            for(int j=0;j<CipherSpecification.roundNumberAndLeftShift[k];j++){
                // When there are 2 number of left shifts then it will go inside this if condition!!!
                if(j!=0){
                    String tempC = c[i];
                    String tempD = d[i];

                    c[i] = tempC.substring(1);
                    c[i] += (tempC.charAt(0));
                    
                    d[i] = tempD.substring(1);
                    d[i] += (tempD.charAt(0));
                }
                else{
                    c[i] = c[i-1].substring(1);
                    c[i] += (c[i-1].charAt(0));
                    d[i] = d[i-1].substring(1);
                    d[i] += (d[i-1].charAt(0)); 
                }
            }           
        }
        String cd[] = new String[16];
        //Initailizing the C1D1 to C16D16!!!
        for(int i=1,j=0;i<17;i++,j++){
            cd[j] = c[i] + d[i];
        }
        return cd;
    }
    static void initailizeK1ToK16(String cd[]){
        // String k[] = new String[16];
        //If we donot do this then in starting it will be null!!!
        // PC2 has 8 rows and 6 columns!!!
        for(int i=0;i<16;i++){
            k[i] = "";
            k[i] += permutationOfMatrix(CipherSpecification.pc2,cd[i]); 
        }
        //By this we have got our 16 keys(i.e K1 to K16)!!!             
    }
    
    
    //This method takes the two Strings as parameter and returns the String which has the answer of Xor of these two Strings !!!
    static String xor(String s1,String s2){
        String ansOfXOR = "";//TO stroe the Answer of XOR of two Strings!!!
        int i=0,j=0;
        while(i!=s1.length()){//Till 'i' is not 32 !!!
            int num1 = Character.getNumericValue(s1.charAt(i));
            int num2 = Character.getNumericValue(s2.charAt(j));
            int xORAns = num1 ^ num2;
            ansOfXOR += Integer.toString(xORAns); 
            i++;
            j++;    
        }
        
        return ansOfXOR;
    }
    static String function(String R,int ind,String SB[],boolean isReverse){
        String er = "";//At the first time it is Er0 and second ER1 and hence on!!!
        String key = k[ind];
        er = permutationOfMatrix(CipherSpecification.expansion,R);
        String kXorEr = xor(er,key);
        //Now in this kXorEr it has to divided into 8 blocks of each 6 bits !!!
        //Now in this 8 blocks which is of 6 bit, if we compute the first and last bit it will give us the number of rows and the remaining middle 4 bits will give us the number of the columns!!!        
        //Now when we got the number of row and number of column then the 1 block has to computed with S1 matrix and second block with S2 matrix and hence on !!!

        int decimalNoOfRows[] = new int[8];
        int decimalNoOfColumns[] = new int[8];
        int j = 0,k = 6;//total 48 bits!!! 
        for(int i=0;i<8;i++,j+=6,k+=6){
            initailizeRowNoAndColumnNo(kXorEr.substring(j,k),i,decimalNoOfRows,decimalNoOfColumns);
        }
        if(isReverse == true){
            initailizeSB((15-ind),SB,decimalNoOfRows,decimalNoOfColumns); // where B is the block of 6 bits !!!         
            return initailizePermutation((15-ind),SB);
        }
        else{
            initailizeSB(ind,SB,decimalNoOfRows,decimalNoOfColumns); // where B is the block of 6 bits !!!      
            return initailizePermutation(ind,SB);
        }
                
    } 
    static void initailizeRowNoAndColumnNo(String input,int indexNo,int decimalNoOfRows[],int decimalNoOfColumns[]){
        String rows = "",Columns = "";
        rows = Character.toString(input.charAt(0));
        rows += (input.charAt(5));
        decimalNoOfRows[indexNo] = binaryToDecimalConverter(rows);
        //decimalNoOfRows[0] will contain the row number of the 1st block!!!
        Columns = input.substring(1,5); // middle 4 bits !!!
        decimalNoOfColumns[indexNo] = binaryToDecimalConverter(Columns);
        //decimalNoOfColumns[0] will contain the column number of the 1st block!!!
    }
    static int binaryToDecimalConverter(String input){
        return Integer.parseInt(input,2);
        //Here the 2 is the Radix Number(i.e for Binary)!!!
    }
    static void initailizeSB(int ind,String SB[],int decimalNoOfRows[],int decimalNoOfColumns[]){
        int rowNo,columnNo;
        for(int i=0;i<8;i++){
            rowNo = decimalNoOfRows[i];
            columnNo = decimalNoOfColumns[i];
            SB[ind] += convertIntoBits(Integer.toString(SubstitutionTables.S[i][rowNo][columnNo]),false);
        }
    }
    //Here the L[0] = L1,R[0] = R1 and hence on!!!

    //Many threads where accessing this method by making it synchronized at a time 
    //only one thread can access the method!!!
    //Control of execution to the next thread when first thread ius finished !!!
    static synchronized String initailizeL16AndR16(String l0,String r0,boolean isReverse){

        String L = "",R = "";
        String SB[] = new String[16];
        if(isReverse == true){
            for(int i=0;i<16;i++){
                SB[i] = "";//To avoid null!!!
                if(i==0){
                    L = r0;
                    R = xor(l0,function(r0,(15-i),SB,isReverse));              
                }else{
                    String Lprev = L;
                    L = R;
                    R = xor(Lprev,function(R,(15-i),SB,isReverse));
                }
            }
        }else{
            for(int i=0;i<16;i++){
                SB[i] = "";//To avoid null!!!
                if(i==0){
                    L = r0;
                    R = xor(l0,function(r0,i,SB,isReverse));              
                }else{
                    String Lprev = L;
                    L = R;
                    R = xor(Lprev,function(R,i,SB,isReverse));
                }
            }    
        }
        
        return swapL16AndR16AndInitailizeIPInverse(L,R);
    }

    

    static String initailizePermutation(int ind,String SB[]){
        return permutationOfMatrix(CipherSpecification.permutation,SB[ind]);
    }
    static String swapL16AndR16AndInitailizeIPInverse(String L,String R){
        String R16L16 = R + L;
        String finalStringInBits = "";
        finalStringInBits = permutationOfMatrix(CipherSpecification.ip_inverse,R16L16);
        String finalString = "";
        int j = 0,k = 4;
        for(int i=0;i<16;i++){
            //finalString += binaryToHexConverter(Integer.parseInt(finalStringInBits.substring(j,k)));
            finalString += binaryToHexConverter(finalStringInBits.substring(j,k));
            j+=4;
            k+=4;   
        }
        return finalString;
    } 
    //This method will take the number as the input and return its equivalent Hexadecimal number!!!
    static String binaryToHexConverter(String number){
        String conversionIntoDecimal = Integer.toString(binaryToDecimalConverter(number));
        int temp = Integer.parseInt(conversionIntoDecimal);
        String tp = Integer.toHexString(temp);
        return tp;
    }

    /**
    * This method will take a String and convert it into the corresponding Hexadecimal!!!
    * @param String: It takes the input string to be converted into the hexadecimal 
    * @return String : The final string containing the hexadecimal answer!!!
    */
    public String convertStringToHex(String str){
      
      char[] chars = str.toCharArray();
      
      StringBuffer hex = new StringBuffer();
      for(int i = 0; i < chars.length; i++){
        hex.append(Integer.toHexString((int)chars[i]));
      }
      System.out.println("hex.toString();"+hex.toString());
      return hex.toString();
    }

    /**
    * This method will take a Hexadecimal and convert it into the corresponding String!!!
    * @param String: It takes the input string which contains hex to be converted into the string 
    * @return String : The final string containing the answer!!!
    */

    public String convertHexToString(String hex){

      StringBuilder sb = new StringBuilder();
      StringBuilder temp = new StringBuilder();
      
      //49204c6f7665204a617661 split into two characters 49, 20, 4c..
      for( int i=0; i<hex.length()-1; i+=2 ){
          
          //grab the hex in pairs
          String output = hex.substring(i, (i + 2));
          //convert hex to decimal
          int decimal = Integer.parseInt(output, 16);
          //convert the decimal to character
          sb.append((char)decimal);
          
          temp.append(decimal);
      }      
      return sb.toString();
  }
}
