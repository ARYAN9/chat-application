/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatsystem.backend.helper.filehandling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class contains all the helper methods for the FileHandling operations (i.e all the methods that will help in the backup operations of client)!!!
 * @author Aryan Dhami
 */
public class FileOperations {
    
    public boolean isFileCreated = false;
    public boolean isClient;
    public String username;
    
    /**
     * This constructor just initialize the members and checks whether the client file is already created or not!!!
     * @param username : The username of the client whose FileHandling has to be done.
     * @param isClient : This is the boolean indicating the whether it is client or server.
     */
    public FileOperations(String username,boolean isClient) {
        this.isClient = isClient;
        this.username = username;
        if(isClient){
            //The below part is for use of the client!!!
            if(new File("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+username+".xml").exists()){
                //Means the file is already created!!!
                this.isFileCreated = true;
            }
        }else{
            //The below part is for use of the server!!!
            if(new File("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+username+".xml").exists()){
                //Means the file is already created!!!
                this.isFileCreated = true;
            }
        }        
    }    
    
    /**
     * This method is use to read the file which path is given and return the contents of the file!!!
     * @param filePath : The String specifying the path of file.
     * @return : String containing the final contents of the file read.
     */
    public synchronized static String readFile(String filePath){
        String str,outputString = "";
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath)))))
        {
            while((str=br.readLine())!=null)
            {
                    outputString = outputString + str;
            }
        }
        catch (IOException ie){
            System.out.println("Error while reading the file:"+ie.getMessage());
        }
        return outputString;//When the exception comes the "" blank string will be returned!!!
    }
    
    /**
     * This method is use to write the contents into the specified file whose path is given by erasing the previous contents in file!!!
     * @param filePath : The String specifying the path of file.
     * @param message : String containing the message that has to be write into the file.
     */
    public synchronized void writeFile(String filePath,String message){

        try(PrintWriter writer = new PrintWriter(new FileOutputStream(new File(filePath)),true))
        {
            writer.println(message);
            this.isFileCreated = true;
        }
        catch (IOException ie){
            System.out.println("Exception in writing of the file:"+ie.getMessage());
        }
    }
    
    /**
     * This method is use to write the contents into the specified file whose path is given by appending to the previous contents in file!!!
     * @param filePath : The String specifying the path of file.
     * @param message : String containing the message that has to be write into the file.
     */
    public synchronized void writeFileAppend(String filePath,String message){

        try(PrintWriter writer = new PrintWriter(new FileOutputStream(new File(filePath),true),true))
        {
            writer.println(message);
            this.isFileCreated = true;
        }
        catch (IOException ie){
            System.out.println("Exception in writing of the file:"+ie.getMessage());
        }
    }

    
    /**
     * This method is use to prevent the spoiling of the backup files by the user by sending the special tags as the message content!!!
     * For ex :- If the user sends "</send>" tag as the message the backup file will be tampered!!!
     * So, this method prevents the tampering!!!
     * @param message : The String containing the message that is to be encoded to prevent tampering.
     * @return : The final String after encoding the message.
     */
    private String encodeSpecialTagsInMessage(String message){
        int index = message.indexOf("<");
        while(index != -1){
            String beforePart = message.substring(0,index);
            String afterPart = message.substring(index);
            message = beforePart + "/" + afterPart;
            index = message.indexOf("<",index+2);
        }
        index = message.indexOf(">");
        while(index != -1){
            String beforePart = message.substring(0,index);
            String afterPart = message.substring(index);
            message = beforePart + "/" + afterPart;
            index = message.indexOf(">",index+2);
        }
        return message;
    }
    
    /**
     * This method is use to prevent the spoiling of the backup files by the user by sending the special tags as the message content!!!
     * This method decodes the same message which is encoded in the above method!!!
     * @param message : The String containing the message that is to be decoded to prevent tampering.
     * @return : The final String after decoding the message.
     */
    public String decodeSpecialTagsInMessage(String message){
        int index = message.indexOf("<");
        while(index != -1){
            if(message.charAt(index-1) == '/'){
                String beforePart = message.substring(0,index-1);
                String afterPart = message.substring(index);
                message = beforePart + afterPart;
            }
            index = message.indexOf("<",index+2);
        }
        index = message.indexOf(">");
        while(index != -1){
            if(message.charAt(index-1) == '/'){
                String beforePart = message.substring(0,index-1);
                String afterPart = message.substring(index);
                message = beforePart + afterPart;
            }
            index = message.indexOf(">",index+2);
        }
        return message;
    }
    
    /**
     * This method is use to delete the file!!!
     * @param filePath : The String specifying the path of the file which has to be deleted.
     */
    public static void deleteFile(String filePath){
        if(new File(filePath).delete()){
            System.out.println("File deleted successfully");
        }else{
            System.out.println("Failed to delete the file:-"+filePath); 
        }
    }
    
    /**
     * This method first takes all the contents of the file and mixes it with the processed content and then again writes that content back to file!!!
     * It finds the proper location where the content needs to be inserted and then writes it!!!
     * It is for all the messages that are received so includes the <receive> tag
     * @param senderName : It is the String containing the name of the sender.
     * @param receiverName : It is the String containing the name of receiver.
     * @param message : It is the actual message between this sender and receiver.
     * @param status : The status of the message in integer.
     *                 If status = 0 [SEND]
     *                 If status = 1 [DELIVERED]
     *                 If status = 2 [READ]
     */
    public synchronized void fillMessageInFileForReceive(String senderName,String receiverName,String message,int status){
        String xmlMessage = "",finalAnswer = ""; 
        message = encodeSpecialTagsInMessage(message);
        if(this.isFileCreated){
            //This means the file is already made and we have to first read, then append and then again rewrite the file!!!
            String previousFileContents;
            if(this.isClient)
                previousFileContents = readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+receiverName+".xml");
            else
                previousFileContents = readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+receiverName+".xml");


            int index = previousFileContents.indexOf("<chat-conv>");
            if(index == -1){
                //The same else block!!!
                //This means the file is not created and first time the file is going to be written!!!
                xmlMessage = "<chat-conv>" + computeXML(senderName, message, status,"receive") + "</chat-conv>";
                xmlMessage = previousFileContents + senderName + "," + xmlMessage;

                this.isFileCreated = true;
                if(isClient)
                    writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+receiverName+".xml", xmlMessage);
                else
                    writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+receiverName+".xml", xmlMessage);
                return;
            }
            if(index == 0){
                previousFileContents = senderName +","+ previousFileContents;
            }else{
                String tempBefore = previousFileContents.substring(0,index);
                String[] tempArray = tempBefore.split(",");
                int i;
                for(i=0;i< tempArray.length;i++){
                    if(tempArray[i].equals(senderName)){
                        break;
                    }
                }
                String msg = "";
                if(i==tempArray.length){
                    msg = senderName + ",";
                }
                String tempAfter = previousFileContents.substring(index);
                previousFileContents = tempBefore + msg + tempAfter;
            }
            
            index = previousFileContents.indexOf("<"+senderName+">");
            if(index == -1){
                //This means that the no conversations with this member !!!
                //This member new tag has to be created!!!
                finalAnswer = previousFileContents.substring(0,previousFileContents.indexOf("</chat-conv>"));
                finalAnswer = finalAnswer + computeXML(senderName, message, status,"receive");
                finalAnswer = finalAnswer + "</chat-conv>";
            }else{
                int lastestCount = getLatestCount(previousFileContents, senderName) + 1;
                xmlMessage = "<message>"+
                             "<receive>"+message+"</receive>"+
                             "<status>"+status+"</status>"+
                             "<msgCount>"+lastestCount+"</msgCount>"+
                             "</message>";
                int nameTagStartIndex = previousFileContents.indexOf("<"+senderName+">")+senderName.length()+2;//<Aryan>
                String beforeFilePart = previousFileContents.substring(0,nameTagStartIndex);//Zero sai <Aryan> tak
                int nameTagEndIndex = previousFileContents.indexOf("</"+senderName+">");//</Aryan>
                String afterFilePart = previousFileContents.substring(nameTagEndIndex);//</Aryan> sai last tak

                //This below string is the actual part in which we have to do modifications!!!
                String actualFilePart = previousFileContents.substring(nameTagStartIndex,nameTagEndIndex);//-(senderName.length()+3)

                actualFilePart = actualFilePart + xmlMessage;
                finalAnswer = beforeFilePart + actualFilePart + afterFilePart;
            }
            
            if(isClient)
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+receiverName+".xml", finalAnswer);
            else
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+receiverName+".xml", finalAnswer);

        }else{
            //This means the file is not created and first time the file is going to be written!!!
            xmlMessage = "<chat-conv>" + computeXML(senderName, message, status,"receive") + "</chat-conv>";
            xmlMessage = senderName + "," + xmlMessage;
            
            this.isFileCreated = true;
            if(isClient)
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+receiverName+".xml", xmlMessage);
            else
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+receiverName+".xml", xmlMessage);
        }
    }
    
    /**
     * The below method will first take the previous all contents of the file and 
     * then find the location where the status needs to be updated and then updates the status at that point and then writes back to the file!!!
     * @param newStatus : The new status that needs to be updated.
     * @param associatedCount : The counter value of the message whose status needs to be updated.
     * @param tagName : The tagName inside which the counter value will be applied to find the message that status has to be updated.
     * @param filePath : The filePath of the file where these messages are stored.
     */
    public synchronized void setStatusinFile(int newStatus,int associatedCount,String tagName,String filePath){  
        String xmlMessage = "<status>"+newStatus+"</status>";
        String previousFileContents = readFile(filePath);
        String pattern = "<"+tagName+">(.*?)</"+tagName+">";
        Pattern p1 = Pattern.compile(pattern);
        Matcher m1 = p1.matcher(previousFileContents);
        String operationalFileContents = "";
        int startIndex = -1,endIndex = -1;
        if(m1.find()){
            startIndex = m1.start();
            endIndex = m1.end();
            operationalFileContents = m1.group(1);
        }
        String beforeFileContent = previousFileContents.substring(0,startIndex+tagName.length()+2);
        String afterFileContent = previousFileContents.substring(endIndex-(tagName.length()+3));//</ARyan>
        int counter = associatedCount;
        pattern = "<status>";
        int index = operationalFileContents.indexOf(pattern);
        while(index != -1){
            counter--;
            if(counter == 0){
                break;
            }
            index = operationalFileContents.indexOf(pattern,index+1);
        }
        //By this index variable contains the index of the tag that needs to be updated!!!
        String finalString = operationalFileContents.substring(0,index);
        finalString = finalString +xmlMessage;
        finalString = finalString + operationalFileContents.substring(operationalFileContents.indexOf("</status>",index)+9);
        
        finalString = beforeFileContent + finalString + afterFileContent;
        
        writeFile(filePath, finalString);
    }
    
    /**This method first takes all the contents of the file and mixes it with the processed content and then again writes that content back to file!!!
     * It finds the proper location where the content needs to be inserted and then writes it!!!
     * It is for all the messages that are send so includes the <send> tag
     * @param senderName : It is the String containing the name of the sender.
     * @param receiverName : It is the String containing the name of receiver.
     * @param message : It is the actual message between this sender and receiver.
     * @param status : The status of the message in integer.
     *                 If status = 0 [SEND] (Default)
     *                 If status = 1 [DELIVERED]
     *                 If status = 2 [READ]
    */
    public synchronized void fillMessageInFileForSend(String senderName,String receiverName,String message,int status){
        String xmlMessage = "",finalAnswer="";
        message = encodeSpecialTagsInMessage(message);
        if(this.isFileCreated){
            //This means the file is already made and we have to first read, then append and then again rewrite the file!!!
            String previousFileContents;
            if(isClient)
                previousFileContents = readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+senderName+".xml");
            else
                previousFileContents = readFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+senderName+".xml");
            
            int index = previousFileContents.indexOf("<chat-conv>");
            if(index == 0){
                previousFileContents = receiverName +","+ previousFileContents;
            }else{
                String tempBefore = previousFileContents.substring(0,index);
                String[] tempArray = tempBefore.split(",");
                int i;
                for(i=0;i< tempArray.length;i++){
                    if(tempArray[i].equals(receiverName)){
                        break;
                    }
                }
                String msg = "";
                if(i==tempArray.length){
                    msg = receiverName + ",";
                }
                String tempAfter = previousFileContents.substring(index);
                previousFileContents = tempBefore + msg + tempAfter;
            }
            
            index = previousFileContents.indexOf("<"+receiverName+">");
            if(index == -1){
                //This means that the no conversations with this member !!!
                //This member new tag has to be created!!!
                finalAnswer = previousFileContents.substring(0,previousFileContents.indexOf("</chat-conv>"));
                finalAnswer = finalAnswer + computeXML(receiverName, message, status,"send");
                finalAnswer = finalAnswer + "</chat-conv>";
            }else{
                int lastestCount = getLatestCount(previousFileContents, receiverName)+1;
                xmlMessage = "<message>"+
                             "<send>"+message+"</send>"+
                             "<status>"+status+"</status>"+
                             "<msgCount>"+lastestCount+"</msgCount>"+
                             "</message>";
                int nameTagStartIndex = previousFileContents.indexOf("<"+receiverName+">")+receiverName.length()+2;//<Aryan>
                String beforeFilePart = previousFileContents.substring(0,nameTagStartIndex);//Zero sai <Aryan> tak
                int nameTagEndIndex = previousFileContents.indexOf("</"+receiverName+">");//</Aryan>
                String afterFilePart = previousFileContents.substring(nameTagEndIndex);//</Aryan> sai last tak

                //This below string is the actual part in which we have to do modifications!!!
                String actualFilePart = previousFileContents.substring(nameTagStartIndex,nameTagEndIndex);//-(receiverName.length()+3)


                actualFilePart = actualFilePart + xmlMessage;
                finalAnswer = beforeFilePart + actualFilePart + afterFilePart;
            }

            if(isClient)
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+senderName+".xml", finalAnswer);
            else
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+senderName+".xml", finalAnswer);

        }else{
            //This means the file is not created and first time the file is going to be written!!!
            xmlMessage = "<chat-conv>" + computeXML(receiverName, message, status,"send") + "</chat-conv>";
            xmlMessage = receiverName + "," + xmlMessage;
            
            this.isFileCreated = true;
            if(isClient)
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\client\\xmlfiles\\"+senderName+".xml", xmlMessage);
            else
                writeFile("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+senderName+".xml", xmlMessage);
        }
    }
    
    /**
     * This method computes the xmlMessage string in special format and returns it!!!
     * @param tagName : The string containing the tagName.
     * @param message : The string containing the actual message.
     * @param status : The integer value indicating the status to putted in XML message.
     * @param messageStatus : The string containing is message "send" or "receive"
     * @return : The final XML string after embedding all the thing.
     */
    public String computeXML(String tagName,String message,int status,String messageStatus){
        String xmlMessage = "<"+tagName+">"+
                            "<message>"+
                            "<"+messageStatus+">"+message+"</"+messageStatus+">"+
                            "<status>"+status+"</status>"+ 
                            "<msgCount>1</msgCount>"+
                            "</message>"+
                            "</"+tagName+">";
//        getLatestCount(, tagName)
        return xmlMessage;    
    }
    
    /**
     * Every message in every tag is associated with the count value when it is made!!!
     * So, this method returns last count in that <tagName></tagName> which is provided!!!
     * @param fileContents : The fileContents from which you want greatest count.
     * @param username : The username is the tagName. 
     * @return : The final integer value of the latest count in fileContent.
     */
    public static synchronized int getLatestCount(String fileContents,String username){
        String pattern = "<"+username+">(.*?)</"+username+">";
        Pattern p1 = Pattern.compile(pattern);
        Matcher m1 = p1.matcher(fileContents);
        String operationalFileContents = "";
        while(m1.find()){
            operationalFileContents = m1.group(1);
        }
        pattern = ".*<msgCount>(.*)</msgCount>";//This will only give the last msgCount tag found!!!
        p1 = Pattern.compile(pattern);
        m1 = p1.matcher(operationalFileContents);
        String msgCountValue = "0";
        while(m1.find()){
            msgCountValue = m1.group(1);
        }
        return Integer.parseInt(msgCountValue);
    }
    
    /**
     * This method is useful when the client is offline and server stores the status of messages in the absence of the client!!!
     * @param senderName : The sender name whose status is to be stored by server.
     * @param isDelivered : The boolean value if true then delivered else read.
     */
    public void updateOfflineClient(String senderName,boolean isDelivered){
        if(isDelivered == true){
            writeFileAppend("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+username+".xml", "D:"+senderName+",");
        }else{
            writeFileAppend("C:\\Users\\Dell\\Desktop\\Java\\chat-application\\chatsystem\\src\\chatsystem\\backend\\server\\xmlfiles\\"+username+".xml", "R:"+senderName+",");
        }
    }
    
    /**
     * This method is responsible for clubbing the client current data with the data that the server had stored for the client in the absence of the client!!!
     * @param pendingMessagesFileData : This is the data the server had stored for client in his absence.
     * @param clientInfo : This is client current data present in the file.
     * @param clientPath : This is path of the client file passed for writing the clubbed data into file.
     * @return : The final string after clubbing the current data of client with the server data.
     */
    public synchronized String putPendingMessagesIntoClientData(String pendingMessagesFileData,String clientInfo,String clientPath){
        String beforePart = pendingMessagesFileData.substring(0,pendingMessagesFileData.indexOf("<chat-conv>"));
        String arrayOfUsers[] = beforePart.split(",");
        for(int i=0;i<arrayOfUsers.length;i++){
            String user = arrayOfUsers[i];
            if(user.contains("R:") || user.contains("D:")){
                continue;
            }
            String pattern = "<"+user+">(.*?)</"+user+">";
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(pendingMessagesFileData);
            String fileContents = "";

            while(m.find()){
                fileContents = m.group(1);
            }

            int index = clientInfo.indexOf("</"+user+">");
            if(index != -1){
                //Means this <user> is there!!!
                String beforePartOfFile = clientInfo.substring(0,index);
                String afterPartOfFile = clientInfo.substring(index);
                writeFile(clientPath,beforePartOfFile + fileContents + afterPartOfFile);
                clientInfo = beforePartOfFile + fileContents + afterPartOfFile;
                
            }else{
                //Means this <user> is not there!!!
                //Putting at the top to indicate!!!
                int tempIndex = clientInfo.indexOf("<chat-conv>");
                String beforeUserPartOfFile = clientInfo.substring(0,tempIndex);
                beforeUserPartOfFile = beforeUserPartOfFile + user + ",";
                
                //Now is appending part!!!
                String beforePartOfFile = clientInfo.substring(tempIndex,clientInfo.indexOf("</chat-conv>"));
                String finalAns = beforeUserPartOfFile + beforePartOfFile+"<"+user+">"+fileContents+"</"+user+">"+"</chat-conv>";
                writeFile(clientPath,finalAns);
                clientInfo = finalAns;
            }
        }
        return clientInfo;
    }
    
    /**
     * This method is use to set the <msgCount></msgCount> tag value of the message that is stored by server so it's message count was not proper!!!
     * As the server does not have the latest message count of the client!!!
     * So, the client with the help of this will set correct message count!!!
     * @param newMsgCount : The new message count to be updated.
     * @param tagName : The tagName inside which we will apply count to get the msgCount.
     * @param filePath : The filePath from which we will extract the tagName's content.
     */
    public synchronized void setMsgCountinFile(int newMsgCount,String tagName,String filePath){  
        String xmlMessage = "<msgCount>"+newMsgCount+"</msgCount>";
        String previousFileContents = readFile(filePath);
        String pattern = "<"+tagName+">(.*?)</"+tagName+">";
        Pattern p1 = Pattern.compile(pattern);
        Matcher m1 = p1.matcher(previousFileContents);
        String operationalFileContents = "";
        int startIndex = -1,endIndex = -1;
        if(m1.find()){
            startIndex = m1.start();
            endIndex = m1.end();
            operationalFileContents = m1.group(1);
        }
        String beforeFileContent = previousFileContents.substring(0,startIndex+tagName.length()+2);
        String afterFileContent = previousFileContents.substring(endIndex-(tagName.length()+3));//</ARyan>
        int counter = newMsgCount;
        pattern = "<msgCount>";
        int index = operationalFileContents.indexOf(pattern);
        while(index != -1){
            counter--;
            if(counter == 0){
                break;
            }
            index = operationalFileContents.indexOf(pattern,index+1);
        }
        //By this index variable contains the index of the tag that needs to be updated!!!
        String finalString = operationalFileContents.substring(0,index);
        finalString = finalString +xmlMessage;
        finalString = finalString + operationalFileContents.substring(operationalFileContents.indexOf("</msgCount>",index)+9);

        finalString = beforeFileContent + finalString + afterFileContent;
        writeFile(filePath, finalString);
    }
}
